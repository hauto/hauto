# HAUTO - Home Automation System

## Documentation

## Installation

To install the HAUTO system:

You need to create a personal access token in gitlab and use it to login to the registry
```bash
docker login registry.gitlab.com/v2/hauto/core -u <your gitlab username> -p <your access token>
```

```bash

git clone git@gitlab.com:hauto/hauto.git

cd hauto


docker-compose pull
docker-compose up -d
```